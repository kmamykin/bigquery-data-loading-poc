python ../generate_sp_api_schema.py \
    https://raw.githubusercontent.com/amzn/selling-partner-api-models/main/models/product-pricing-api-model/productPricingV0.json \
    GetOffersResult \
    --no-envelope \
    > ./item_offers_table_schema.json 

python ../generate_sp_api_schema.py \
    https://raw.githubusercontent.com/amzn/selling-partner-api-models/main/models/product-pricing-api-model/productPricingV0.json \
    GetOffersResult \
    --no-envelope \
    --external-table-source-uri="gs://sp_api/Products/item_offers/*.json" \
    > ./item_offers_external_table_definition.json 
