bq load \
  --replace \
  --source_format=NEWLINE_DELIMITED_JSON \
  --ignore_unknown_values \
  data_loading_poc.sp_api_item_offers \
  gs://sp_api/Products/item_offers/*.json \
  ./item_offers_table_schema.json