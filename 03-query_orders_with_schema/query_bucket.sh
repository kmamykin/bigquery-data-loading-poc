bq query \
  --external_table_definition=tbl::orders_as_jsonl.json \
  '
    select 
      _FILE_NAME as fn,
      date, 
      country,
      orders.AmazonOrderId,
      orders.SellerOrderId,
      orders.LastUpdateDate,
      orders.OrderStatus,
      orders.FulfillmentChannel,
      orders.SalesChannel,
      orders.OrderChannel,
      orders.ShipServiceLevel,
      orders.OrderTotal.Amount as OrderTotal_Amount,
      orders.OrderTotal.CurrencyCode as OrderTotal_CurrencyCode,
      orders.NumberOfItemsShipped,
      orders.NumberOfItemsUnshipped
    from tbl
    left join unnest(tbl.data) data
    left join unnest(data.Orders) orders
  '
