with unnested_events as (

    select
        se.AmazonOrderId,
        se.SellerOrderId,
        se.MarketplaceName,
        se.PostedDate,
        se.ShipmentItemList,
        se.ShipmentItemAdjustmentList,
        se.OrderChargeList,
        se.OrderChargeAdjustmentList,
        se.ShipmentFeeList,
        se.ShipmentFeeAdjustmentList,
        se.OrderFeeList,
        se.OrderFeeAdjustmentList,
        se.DirectPaymentList

    from {{ ref('finance_events') }}
    cross join unnest(ShipmentEventList) as se

)

select *
from unnested_events
