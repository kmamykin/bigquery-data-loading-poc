with shipment_events as (

    select * from {{ ref('finance_shipment_events') }}

),

shipment_items as (

    select
        se.AmazonOrderId,
        se.SellerOrderId,
        se.MarketplaceName,
        se.PostedDate,
        sil.SellerSKU,
        sil.OrderItemId,
        sil.OrderAdjustmentItemId,
        sil.QuantityShipped,
        sil.ItemChargeList,
        sil.ItemChargeAdjustmentList,
        sil.ItemFeeList,
        sil.ItemFeeAdjustmentList,
        sil.ItemTaxWithheldList,
        sil.PromotionList,
        sil.PromotionAdjustmentList,
        sil.CostOfPointsGranted,
        sil.CostOfPointsReturned
    from shipment_events as se
    cross join unnest(se.ShipmentItemList) as sil

)

select *
from shipment_items