
{{ config(materialized='view') }}

with raw_json_api_responses as (

    select
        date,
        data
    from {{ source('datalake', 'finance_events') }}
    -- TODO: Deduplication to pick up latest records happens here
    -- TODO: How to join to merchant? by merchant slug?

)

select
    date as api_query_date,
    d.FinancialEvents.ShipmentEventList,
    d.FinancialEvents.RefundEventList,
    d.FinancialEvents.GuaranteeClaimEventList,
    d.FinancialEvents.ChargebackEventList,
    d.FinancialEvents.PayWithAmazonEventList,
    d.FinancialEvents.ServiceProviderCreditEventList,
    d.FinancialEvents.RetrochargeEventList,
    d.FinancialEvents.RentalTransactionEventList,
    d.FinancialEvents.ProductAdsPaymentEventList,
    d.FinancialEvents.ServiceFeeEventList,
    d.FinancialEvents.SellerDealPaymentEventList,
    d.FinancialEvents.DebtRecoveryEventList,
    d.FinancialEvents.LoanServicingEventList,
    d.FinancialEvents.AdjustmentEventList,
    d.FinancialEvents.SAFETReimbursementEventList,
    d.FinancialEvents.SellerReviewEnrollmentPaymentEventList,
    d.FinancialEvents.FBALiquidationEventList,
    d.FinancialEvents.CouponPaymentEventList,
    d.FinancialEvents.ImagingServicesFeeEventList,
    d.FinancialEvents.NetworkComminglingTransactionEventList,
    d.FinancialEvents.AffordabilityExpenseEventList,
    d.FinancialEvents.AffordabilityExpenseReversalEventList
from raw_json_api_responses
cross join unnest(raw_json_api_responses.data) d
