python ../generate_sp_api_schema.py \
    https://raw.githubusercontent.com/amzn/selling-partner-api-models/main/models/orders-api-model/ordersV0.json \
    OrdersList \
    > ./orders_table_schema.json 

python ../generate_sp_api_schema.py \
    https://raw.githubusercontent.com/amzn/selling-partner-api-models/main/models/orders-api-model/ordersV0.json \
    OrdersList \
    --external-table-source-uri="gs://sp_api/orders/*.json" \
    > ./orders_external_table_definition.json 
