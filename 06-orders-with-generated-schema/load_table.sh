bq load \
  --replace \
  --source_format=NEWLINE_DELIMITED_JSON \
  --ignore_unknown_values \
  data_loading_poc.sp_api_orders_responses \
  gs://sp_api/orders/*.json \
  ./orders_table_schema.json