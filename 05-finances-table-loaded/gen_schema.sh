python ../generate_sp_api_schema.py \
    https://raw.githubusercontent.com/amzn/selling-partner-api-models/main/models/finances-api-model/financesV0.json \
    ListFinancialEventsPayload \
    --fix-finances-schema \
    > ./finances_table_schema.json 
