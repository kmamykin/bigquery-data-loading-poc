bq load \
  --replace \
  --source_format=NEWLINE_DELIMITED_JSON \
  --ignore_unknown_values \
  data_loading_poc.sp_api_finance_events \
  gs://sp_api/finance/*.json \
  ./finances_table_schema.json