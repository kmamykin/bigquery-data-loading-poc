
import argparse
from prance import ResolvingParser
import json
import pathlib


def bigquery_external_table_definition(sourceUri, fields):
    # ignoreUnknownValues is important to not fail when an unknown field shows up in the raw JSON.
    # The schema should take case of all documented fields, but we can't rely 100% on it's completeness.
    return {
        "sourceUris": [sourceUri],
        "sourceFormat": "NEWLINE_DELIMITED_JSON",
        "schema": {
            "fields": fields
        },
        "ignoreUnknownValues": True
    }


def bigquery_table_schema_with_envelope(fields):
    return [
        {
            "name": "date",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "country",
            "type": "STRING",
            "mode": "NULLABLE"
        },
        {
            "name": "is_branded",
            "type": "BOOL",
            "mode": "NULLABLE"
        },
        {
            "name": "data",
            "type": "RECORD",
            "mode": "REPEATED",
            "fields": fields
        }
    ]


def table_schema_for_openapi_definition(name, object_spec):
    """
    Traverses OpenAPI object specification top-down and outputs a dictionary 
    in the format of BigQuery table schema.
    Parameters:
    name - the name of field to output in table schema (normally it is the key of a field/property in OpenAPI)
    object_spec - dictionary of dictionaries in OpenAPI specification format
    """
    #print(name, object_spec.keys())
    type_conversions = {
        "integer": "INTEGER",
        "number": "NUMERIC",
        "string": "STRING",
        "boolean": "BOOL"
    }
    #description = object_spec.get("description", "")
    def_type = str(object_spec["type"]).lower()
    assert def_type in ["object", "array"] + list(type_conversions.keys()), f"Can not convert {def_type} type"
    if def_type == "object":
        return {
            "name": name,
            #"description": description,
            "type": "RECORD",
            "mode": "NULLABLE",
            "fields": [table_schema_for_openapi_definition(key, value) for key, value in object_spec['properties'].items()
                        if key != "sellerId"] # HACK: some objects define both 'sellerId' and 'SellerId' props on the same object, BQ does not like it. 
        }
    elif def_type == "array":
        items_schema = table_schema_for_openapi_definition(name, object_spec['items'])
        items_schema.update({"mode": "REPEATED"})
        return items_schema
    else:
        return {
            "name": name,
            #"description": description,
            "type": type_conversions[def_type],
            "mode": "NULLABLE"
        }

def fix_finances_schema_with_extra_fields(data, extras):
    def find_by_name(arr, name):
        return next(e for e in arr if e["name"] == name)
    node = find_by_name(data["fields"], "FinancialEvents")
    node["fields"] = extras + node["fields"]
    return data

def main(args):
    parser = ResolvingParser(args.openapi_spec_uri)
    data = table_schema_for_openapi_definition(args.definition, parser.specification['definitions'][args.definition])
    fields = data["fields"]

    if args.fix_finances_schema:
        fixes_path = pathlib.Path(__file__).parent.resolve() / 'finances_undocumented_event_fields.json'
        with open(fixes_path, 'r') as f:
            extra_fields = json.load(f)
        data = fix_finances_schema_with_extra_fields(data, extra_fields)

    if args.external_table_source_uri:
        if args.envelope:
            result = bigquery_external_table_definition(args.external_table_source_uri, bigquery_table_schema_with_envelope(fields))
        else:
            result = bigquery_external_table_definition(args.external_table_source_uri, fields)
    else:
        if args.envelope:
            result = bigquery_table_schema_with_envelope(fields)
        else:
            result = fields
    print(json.dumps(result, indent=4))


if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser()
    parser.add_argument("openapi_spec_uri", help="URI of the OpenAPI spec file")
    parser.add_argument("definition", help="The name of the definition in the OpenAPI spec file that maps to the JSON envelope 'data' element")
    parser.add_argument("--external-table-source-uri", help="When specified outputs an external table definition pointing to the data at this location")
    parser.add_argument("--fix-finances-schema", action="store_true")
    parser.add_argument("--envelope", dest='envelope', action='store_true', help="To wrap the schema in a custom envelope")
    parser.add_argument("--no-envelope", dest='envelope', action='store_false', help="Not to wrap the schema in a custom envelope")
    parser.set_defaults(envelope=True)
    args = parser.parse_args()
    main(args)