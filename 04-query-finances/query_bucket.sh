bq query \
  --external_table_definition=tbl::finances_table_def.json \
  '
    select 
      _FILE_NAME as fn,
      date, 
      country,
      ShipmentEventList.AmazonOrderId,
      ShipmentEventList.SellerOrderId,
      ShipmentEventList.MarketplaceName,
      ShipmentEventList.PostedDate,
      ShipmentItemList.SellerSKU,
      ShipmentItemList.OrderItemId,
      ShipmentItemList.QuantityShipped,
      ItemChargeList.ChargeType,
      ItemChargeList.ChargeAmount
    from tbl
    cross join unnest(tbl.data) data
    cross join unnest(data.FinancialEvents) FinancialEvents
    cross join unnest(FinancialEvents.ShipmentEventList) ShipmentEventList
    left join unnest(ShipmentEventList.ShipmentItemList) ShipmentItemList
    left join unnest(ShipmentItemList.ItemChargeList) ItemChargeList
  '
